""" Viewlets related to application logic.
"""

# Zope imports
from zope.interface import Interface
from five import grok
import json

# Search for templates in the 'templates' directory
grok.templatedir('templates')

class MyView(grok.View):
    """ Render the title and description of item only (example)
    """

    # The view is available on every content item type
    grok.context(Interface)

    def title_or_description(self):
    	#import pdb;pdb.set_trace()
    	if self.context.Title():
    		return self.context.Title()

    	return self.context.Description()

class JsonData(grok.View):
    """ Render the title and description of item only (example)
    """

    # The view is available on every content item type
    grok.context(Interface)

    def render(self):
	return json.dumps({'id': self.context.id,
                           'title': self.context.Title()})

