from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

from plone.testing import z2

from zope.configuration import xmlconfig


class VedemoLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import ve.demo
        xmlconfig.file(
            'configure.zcml',
            ve.demo,
            context=configurationContext
        )

        # Install products that use an old-style initialize() function
        #z2.installProduct(app, 'Products.PloneFormGen')

#    def tearDownZope(self, app):
#        # Uninstall products installed above
#        z2.uninstallProduct(app, 'Products.PloneFormGen')

    def setUpPloneSite(self, portal):
        applyProfile(portal, 've.demo:default')

VE_DEMO_FIXTURE = VedemoLayer()
VE_DEMO_INTEGRATION_TESTING = IntegrationTesting(
    bases=(VE_DEMO_FIXTURE,),
    name="VedemoLayer:Integration"
)
VE_DEMO_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(VE_DEMO_FIXTURE, z2.ZSERVER_FIXTURE),
    name="VedemoLayer:Functional"
)
