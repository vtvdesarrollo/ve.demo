from  ve.demo.testing import VE_DEMO_FUNCTIONAL_TESTING
from plone.testing import layered
import robotsuite
import unittest


def test_suite():
    suite = unittest.TestSuite()
    suite.addTests([
        layered(robotsuite.RobotTestSuite("robot_test.txt"),
                layer=VE_DEMO_FUNCTIONAL_TESTING)
    ])
    return suite